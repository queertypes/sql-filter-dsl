module Data.Sql.Filter (
  -- * Types
  SqlLiteral,
  SqlTerm,
  SqlBinOp,
  SqlFilterExpr,

  -- * Literals
  int8l,
  int4l,
  int2l,
  serial8l,
  serial4l,
  serial2l,
  uuidl,
  bytesl,
  textl,
  timestampl,
  timestamptzl,
  booll,

  -- * Terms
  term,
  var,
  int8,
  int4,
  int2,
  serial8,
  serial4,
  serial2,
  uuid,
  bytes,
  text,
  timestamp,
  timestamptz,
  bool,

  -- * Array literals
  int8s,
  int4s,
  int2s,
  uuids,
  bytess,
  texts,
  timestamps,
  timestamptzs,
  bools,

  -- * Binary operators
  lt,
  (.<.),
  gt,
  (.>.),
  lte,
  (.<=.),
  gte,
  (.>=.),
  eq,
  (.==.),
  neq,
  (./=.),
  in',

  -- * Logical expressions
  and,
  (.&&.),
  or,
  (.||.),
  not,
  (.!.),

  -- * Compiler
  compile
) where

import Prelude hiding (and, or, not)

import Data.Sql.Filter.Internal
