{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleContexts #-}

module Data.Sql.Filter.Internal where

import Data.List (intercalate)
import Data.Word
import Data.Int
import Data.ByteString (ByteString)
import Data.Text (Text)
import Data.UUID (UUID, toString)
import Data.Time.Clock (UTCTime)
import Data.Time.LocalTime (ZonedTime)
import qualified Data.ByteString.Base16 as B
import qualified Data.ByteString.Char8 as BC
import qualified Data.Text as T

-- ========================================================================== --
                        -- Language Design --
-- ========================================================================== --
data SqlLiteral a where
  SqlInt8        :: Int64 -> SqlLiteral Int64
  SqlInt4        :: Int32 -> SqlLiteral Int32
  SqlInt2        :: Int16 -> SqlLiteral Int16
  SqlSerial8     :: Word64 -> SqlLiteral Word64
  SqlSerial4     :: Word32 -> SqlLiteral Word32
  SqlSerial2     :: Word16 -> SqlLiteral Word16
  SqlUUID        :: UUID -> SqlLiteral UUID
  SqlBytes       :: ByteString -> SqlLiteral ByteString
  SqlText        :: Text -> SqlLiteral Text
  SqlTimeStamp   :: UTCTime -> SqlLiteral UTCTime
  SqlTimeStampTz :: ZonedTime -> SqlLiteral ZonedTime
  SqlBool        :: Bool -> SqlLiteral Bool

data SqlTerm a where
  SqlVar         :: Text -> SqlTerm a
  SqlLit         :: SqlLiteral a -> SqlTerm a
  SqlArray       :: [SqlLiteral a]  -> SqlTerm [a]

data SqlBinOp a where
  SqlLt  :: SqlTerm a -> SqlTerm a -> SqlBinOp Bool
  SqlGt  :: SqlTerm a -> SqlTerm a -> SqlBinOp Bool
  SqlLte :: SqlTerm a -> SqlTerm a -> SqlBinOp Bool
  SqlGte :: SqlTerm a -> SqlTerm a -> SqlBinOp Bool
  SqlEq  :: SqlTerm a -> SqlTerm a -> SqlBinOp Bool
  SqlNeq :: SqlTerm a -> SqlTerm a -> SqlBinOp Bool
  In     :: SqlTerm a -> SqlTerm [a] -> SqlBinOp Bool
  -- Any    :: (SqlTerm a -> SqlBinOp Bool) -> [SqlLiteral a] -> SqlBinOp Bool
  -- All    :: (SqlTerm a -> SqlBinOp Bool) -> [SqlLiteral a] -> SqlBinOp Bool

data SqlFilterExpr a where
  -- Type-precise Literals
  Term :: SqlTerm a -> SqlFilterExpr a

  -- Logical operators
  And :: SqlFilterExpr Bool -> SqlFilterExpr Bool -> SqlFilterExpr Bool
  Or  :: SqlFilterExpr Bool -> SqlFilterExpr Bool -> SqlFilterExpr Bool
  Not :: SqlFilterExpr Bool -> SqlFilterExpr Bool

  -- Comparison operators, defined over all Sql types
  BinOp :: SqlBinOp a -> SqlFilterExpr Bool

-- ========================================================================== --
           -- Lifting Functions; Combinators --
-- ========================================================================== --
--------------------------------------------------------------------------------
                            -- Literals --
--------------------------------------------------------------------------------
int8l        :: Int64 -> SqlLiteral Int64
int4l        :: Int32 -> SqlLiteral Int32
int2l        :: Int16 -> SqlLiteral Int16
serial8l     :: Word64 -> SqlLiteral Word64
serial4l     :: Word32 -> SqlLiteral Word32
serial2l     :: Word16 -> SqlLiteral Word16
uuidl        :: UUID -> SqlLiteral UUID
bytesl       :: ByteString -> SqlLiteral ByteString
textl        :: Text -> SqlLiteral Text
timestampl   :: UTCTime -> SqlLiteral UTCTime
timestamptzl :: ZonedTime -> SqlLiteral ZonedTime
booll        :: Bool -> SqlLiteral Bool

int8l = SqlInt8
int4l = SqlInt4
int2l = SqlInt2

serial8l = SqlSerial8
serial4l = SqlSerial4
serial2l = SqlSerial2

uuidl = SqlUUID
bytesl = SqlBytes
textl = SqlText
timestampl = SqlTimeStamp
timestamptzl = SqlTimeStampTz
booll = SqlBool

--------------------------------------------------------------------------------
                             -- Terms --
--------------------------------------------------------------------------------
term        :: SqlTerm a -> SqlFilterExpr a
var         :: Text  -> SqlTerm a
int8        :: Int64 -> SqlTerm Int64
int4        :: Int32 -> SqlTerm Int32
int2        :: Int16 -> SqlTerm Int16
serial8     :: Word64 -> SqlTerm Word64
serial4     :: Word32 -> SqlTerm Word32
serial2     :: Word16 -> SqlTerm Word16
uuid        :: UUID -> SqlTerm UUID
bytes       :: ByteString -> SqlTerm ByteString
text        :: Text -> SqlTerm Text
timestamp   :: UTCTime -> SqlTerm UTCTime
timestamptz :: ZonedTime -> SqlTerm ZonedTime
bool        :: Bool -> SqlTerm Bool

int8s :: [Int64] -> SqlTerm [Int64]
int4s :: [Int32] -> SqlTerm [Int32]
int2s :: [Int16] -> SqlTerm [Int16]
uuids :: [UUID] -> SqlTerm [UUID]
bytess :: [ByteString] -> SqlTerm [ByteString]
texts :: [Text] -> SqlTerm [Text]
timestamps :: [UTCTime] -> SqlTerm [UTCTime]
timestamptzs :: [ZonedTime] -> SqlTerm [ZonedTime]
bools :: [Bool] -> SqlTerm [Bool]

-- NOTE: type serial[] does not exist for postgresql
int8s = SqlArray . map int8l
int4s = SqlArray . map int4l
int2s = SqlArray . map int2l
uuids = SqlArray . map uuidl
bytess = SqlArray . map bytesl
texts = SqlArray . map textl
timestamps = SqlArray . map timestampl
timestamptzs = SqlArray . map timestamptzl
bools = SqlArray . map booll

term = Term
var = SqlVar
int8 = SqlLit . SqlInt8
int4 = SqlLit . SqlInt4
int2 = SqlLit . SqlInt2

serial8 = SqlLit . SqlSerial8
serial4 = SqlLit . SqlSerial4
serial2 = SqlLit . SqlSerial2

uuid = SqlLit . SqlUUID
bytes = SqlLit . SqlBytes
text = SqlLit . SqlText
timestamp = SqlLit . SqlTimeStamp
timestamptz = SqlLit . SqlTimeStampTz
bool = SqlLit . SqlBool
-- array = SqlArray

--------------------------------------------------------------------------------
                        -- Binary Operators --
--------------------------------------------------------------------------------
lt    :: SqlTerm a -> SqlTerm a -> SqlFilterExpr Bool
(.<.)  :: SqlTerm a -> SqlTerm a -> SqlFilterExpr Bool
gt    :: SqlTerm a -> SqlTerm a -> SqlFilterExpr Bool
(.>.)  :: SqlTerm a -> SqlTerm a -> SqlFilterExpr Bool
lte   :: SqlTerm a -> SqlTerm a -> SqlFilterExpr Bool
(.<=.) :: SqlTerm a -> SqlTerm a -> SqlFilterExpr Bool
gte   :: SqlTerm a -> SqlTerm a -> SqlFilterExpr Bool
(.>=.) :: SqlTerm a -> SqlTerm a -> SqlFilterExpr Bool
eq    :: SqlTerm a -> SqlTerm a -> SqlFilterExpr Bool
(.==.) :: SqlTerm a -> SqlTerm a -> SqlFilterExpr Bool
neq   :: SqlTerm a -> SqlTerm a -> SqlFilterExpr Bool
(./=.) :: SqlTerm a -> SqlTerm a -> SqlFilterExpr Bool
in'  :: SqlTerm a -> SqlTerm [a] -> SqlFilterExpr Bool
-- any :: (SqlTerm a -> SqlBinOp Bool) -> [SqlLiteral a] -> SqlBinOp Bool
-- all :: (SqlTerm a -> SqlBinOp Bool) -> [SqlLiteral a] -> SqlBinOp Bool

lt x y    = BinOp $ SqlLt x y
(.<.) x y = BinOp $ SqlLt x y

gt x y    = BinOp $ SqlGt x y
(.>.) x y = BinOp $ SqlGt x y

lte x y    = BinOp $ SqlLte x y
(.<=.) x y = BinOp $ SqlLte x y

gte x y    = BinOp $ SqlGte x y
(.>=.) x y = BinOp $ SqlGte x y

eq x y     = BinOp $ SqlEq x y
(.==.) x y = BinOp $ SqlEq x y

neq = (./=.)
(./=.) x y = BinOp $ SqlNeq x y

in' x xs = BinOp $ In x xs
-- any = Any
-- all = All

--------------------------------------------------------------------------------
                     -- Expression Combinators --
--------------------------------------------------------------------------------
and   :: SqlFilterExpr Bool -> SqlFilterExpr Bool -> SqlFilterExpr Bool
(.&&.) :: SqlFilterExpr Bool -> SqlFilterExpr Bool -> SqlFilterExpr Bool
or    :: SqlFilterExpr Bool -> SqlFilterExpr Bool -> SqlFilterExpr Bool
(.||.) :: SqlFilterExpr Bool -> SqlFilterExpr Bool -> SqlFilterExpr Bool
not   :: SqlFilterExpr Bool -> SqlFilterExpr Bool
(.!.)  :: SqlFilterExpr Bool -> SqlFilterExpr Bool


and = And
(.&&.) = And

or = Or
(.||.) = Or

not = Not
(.!.) = Not

--------------------------------------------------------------------------------
                     -- Operator Fixity --
--------------------------------------------------------------------------------
infixr 2 .||.
infixr 3 .&&.
infix 4 .<.
infix 4 .>.
infix 4 .<=.
infix 4 .>=.
infix 4 .==.
infix 4 ./=.
infix 4 `in'`

-- ========================================================================== --
                 -- Compilation and Interpretation --
-- ========================================================================== --
escape :: String -> String
escape x = "\'" ++ x ++ "\'"

repr' :: SqlTerm a -> String
repr' (SqlVar x) = "?" ++ T.unpack x
repr' (SqlLit x) = repr x
repr' (SqlArray x) = "{" ++ intercalate "," (map repr x) ++ "}"

repr :: SqlLiteral a -> String
repr (SqlInt8 x) = show x
repr (SqlInt4 x) = show x
repr (SqlInt2 x) = show x
repr (SqlSerial8 x) = show x
repr (SqlSerial4 x) = show x
repr (SqlSerial2 x) = show x
repr (SqlUUID x) = escape . toString $ x
repr (SqlBytes x) = escape . BC.unpack . B.encode $ x
repr (SqlText x) = escape . T.unpack $ x
repr (SqlTimeStamp x) = escape . show $ x
repr (SqlTimeStampTz x) = escape . show $ x
repr (SqlBool x) = show x

repr'' :: SqlBinOp a -> String
repr'' (SqlLt l r)  = repr' l ++ " " ++ "<" ++ " " ++ repr' r
repr'' (SqlGt l r)  = repr' l ++ " " ++ ">" ++ " " ++ repr' r
repr'' (SqlLte l r) = repr' l ++ " " ++ "<=" ++ " " ++ repr' r
repr'' (SqlGte l r) = repr' l ++ " " ++ ">=" ++ " " ++ repr' r
repr'' (SqlEq l r)  = repr' l ++ " " ++ "=" ++ " " ++ repr' r
repr'' (SqlNeq l r) = repr' l ++ " " ++ "<>" ++ " " ++ repr' r
repr'' (In l r)     = repr' l ++ " " ++ "IN" ++ " " ++ repr' r
-- repr'' (Any l r) = undefined
-- repr'' (All l r) = undefined

compile :: SqlFilterExpr Bool -> String
compile (Term x) = repr' x
compile (BinOp x) = repr'' x
compile (And l r) = compile l ++ " AND " ++ compile r
compile (Or  l r) = compile l ++ " OR " ++ compile r
compile (Not x) = "NOT " ++ compile x
