# 0.1.0.1 (Sep. 18, 2015)

* Compile only expressions that would eval to Bool

# 0.1.0.0 (Sep. 17, 2015)

* Start everything: basic language, compile, types, operators
