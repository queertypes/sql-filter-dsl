# SQL Filter

An experimental DSL for generating postgresql-compatible filters
safely.

Here's an early example:

```haskell
> :set -XOverloadedStrings
> compile (var "x" .<. int4 10 .&&. var "x" .>. int4 3)
?x < 10 AND ?x > 3
> compile (text "cat" .<. bytes "cat")

<interactive>:8:25:
Couldn't match type ‘Data.ByteString.Internal.ByteString’
with ‘Data.Text.Internal.Text’
Expected type: SqlTerm Data.Text.Internal.Text
Actual type: SqlTerm Data.ByteString.Internal.ByteString
In the second argument of ‘(.<.)’, namely ‘bytes "cat"’
In the first argument of ‘compile’, namely
      ‘(text "cat" .<. bytes "cat")’
```

It provides some type safety and a careful mapping of Haskell types to
postgresql types.

Some missing features:

* Type-checking `var` expressions
  * The type of a `var` should be inferred based on context
  * Currently allows for wrong things like: `var "x" < int4 10 .&&. var "x" < bytes "cat"`
* Array expressions: `in`, `all`, `any`
